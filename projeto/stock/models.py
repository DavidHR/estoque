from django.db import models
from ckeditor .fields import RichTextField
from django.contrib.auth.models import User

class Product(models.Model):
    name = models.CharField(max_length=45)
    description = models.TextField()
    code = models.CharField(max_length=20)
    image = models.ImageField(null=True, upload_to='products')
    amount = models.PositiveIntegerField()


    def __str__(self):
        return self.code + ' - ' + self.name